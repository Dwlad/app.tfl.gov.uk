import * as React from "react";
export function PassPropsToChild(Component, props, ...additional) {
	let result = Object.assign({}, ...additional, props);
	return <Component {...result} />;
}
