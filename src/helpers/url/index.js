import { API_URL, APP_ID, APP_KEYS } from "../../constants/config";
import axios from "axios";

export class URL {
	static get(path) {
        const divider = path.includes("?") ? "&" : "?";
		return `${API_URL}${path}${divider}app_id=${APP_ID}&app_key=${APP_KEYS}`;
	}
	static request(path) {
		return axios
			.get(URL.get(path))
			.then(res => res.data)
	}
}
