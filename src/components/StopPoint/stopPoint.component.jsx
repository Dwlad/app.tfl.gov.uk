import React, { PureComponent } from "react";
import { NavLink } from "react-router-dom";
import { MapsComponent } from "../Maps/maps.component";

import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Paper from "material-ui/Paper";
import Grid from "material-ui/Grid";
import Typography from "material-ui/Typography";
import List, { ListItem, ListItemIcon, ListItemText } from "material-ui/List";
import DirectionsBus from "material-ui-icons/DirectionsBus";
import { CircleLoader } from "react-spinners";

import moment from "moment";
import "moment/locale/ru";

moment.locale("ru");

const styles = theme => ({
	root: {
		flexGrow: 1,
		padding: 16,
		margin: theme.spacing.unit * 3 + "px auto",
		width: "400px",
		[theme.breakpoints.down("sm")]: {
			width: "85%"
		},
		textAlign: "center"
	},
	paper: {
		padding: 16,
		textAlign: "center",
		color: theme.palette.text.secondary
	},
	preloader: {
		display: "flex",
		flex: "0 0 auto",
		alignItems: "center",
		justifyContent: "center",
		margin: "20px"
	}
});

class StopPoint extends PureComponent {
	componentWillMount() {
		const { stopPointActions, match, arrivalsActions } = this.props;
		stopPointActions.getStopPointById(match.params.id);
		arrivalsActions.getArrivalForLineByStopPoint(match.params.id);
	}
	componentWillUpdate(nextProps) {
		const { stopPointActions, match, arrivalsActions } = this.props;

		if (match.params.id !== nextProps.match.params.id) {
			stopPointActions.clearStopPoint();
			arrivalsActions.clearArrivals();

			stopPointActions.getStopPointById(nextProps.match.params.id);
			arrivalsActions.getArrivalForLineByStopPoint(
				nextProps.match.params.id
			);
		}
	}

	componentWillUnmount() {
		const { stopPointActions, arrivalsActions } = this.props;
		stopPointActions.clearStopPoint();
		arrivalsActions.clearArrivals();
	}
	render() {
		const { stopPoint, arrivals, classes } = this.props;

		let result = null;
		if (stopPoint.data) {
			result = (
				<div>
					<div className={classes.preloader}>
						<CircleLoader
							color={"#3f51b5"}
							loading={stopPoint.loading}
						/>
					</div>
					{stopPoint.data && (
						<div>
							<Paper className={classes.root} elevation={4}>
								<Typography type="headline" component="h2">
									Stoppoint: {stopPoint.data.commonName}
								</Typography>
							</Paper>
							<Grid container spacing={24}>
								<Grid item xs={12} sm={6}>
									<List>
										{stopPoint.data.lines.map(
											(line, i, arr) => (
												<ListItem
													button
													component={NavLink}
													to={`/route/${line.id}`}
													key={line.id}
												>
													<ListItemIcon>
														<DirectionsBus />
													</ListItemIcon>
													<ListItemText
														primary={line.name}
													/>
													{arrivals.data.length > 0 &&
														arrivals.data
															.filter(
																arrival =>
																	arrival.lineId ===
																	line.id
															)
															.slice(0, 1)
															.map(arrival => (
																<span
																	key={
																		arrival.id
																	}
																>
																	Next bus:{" "}
																	{moment(
																		arrival.expectedArrival
																	).format(
																		"LT"
																	)}
																</span>
															))}
												</ListItem>
											)
										)}
									</List>
								</Grid>
								<Grid item xs={12} sm={6}>
									<MapsComponent
										containerElement={
											<div style={{ height: `400px` }} />
										}
										mapElement={
											<div style={{ height: `400px` }} />
										}
										lng={stopPoint.data.lon}
										lat={stopPoint.data.lat}
									/>
								</Grid>
							</Grid>
						</div>
					)}
				</div>
			);
		}
		return result;
	}
}

StopPoint.propTypes = {
	classes: PropTypes.object.isRequired
};

export const StopPointComponent = withStyles(styles)(StopPoint);
