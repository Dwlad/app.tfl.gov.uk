import React, { PureComponent } from "react";
import { RouteSequenceComponent } from "../RouteSequence/routeSequence.component";

import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Paper from "material-ui/Paper";
import Typography from "material-ui/Typography";
import { CircleLoader } from "react-spinners";

const styles = theme => ({
	root: theme.mixins.gutters({
		paddingTop: 16,
		paddingBottom: 16,
		marginTop: theme.spacing.unit * 3,
		width: "400px",
		[theme.breakpoints.down("sm")]: {
			width: "85%"
		},
		margin: "0 auto",
		textAlign: "center"
	}),
	preloader: {
		display: "flex",
		flex: "0 0 auto",
		alignItems: "center",
		justifyContent: "center",
		margin: "20px"
	}
});

export class Route extends PureComponent {
	componentWillMount() {
		const { match, routeActions, routeSequenceActions } = this.props;
		routeActions.getRouteById(match.params.id);
		routeSequenceActions.getRouteSequenceByLine(match.params.id);
	}

	componentWillUpdate(nextProps) {
		const { match, routeActions, routeSequenceActions } = this.props;
		if (match.params.id !== nextProps.match.params.id) {
			this.props.routeActions.clearRoute();
			this.props.routeSequenceActions.clearRouteSequence();

			routeActions.getRouteById(nextProps.match.params.id);
			routeSequenceActions.getRouteSequenceByLine(
				nextProps.match.params.id
			);
		}
	}

	componentWillUnmount() {
		this.props.routeActions.clearRoute();
		this.props.routeSequenceActions.clearRouteSequence();
	}
	render() {
		const { route, routeSequence, classes } = this.props;

		let result = null;
		if (route.data) {
			result = (
				<div>
					<div className={classes.preloader}>
						<CircleLoader
							color={"#3f51b5"}
							loading={route.loading}
						/>
					</div>
					{route.data && (
						<div>
							<Paper className={classes.root} elevation={4}>
								<Typography type="headline" component="h3">
									Bus №{route.data.name}
								</Typography>
								<Typography component="p">
									Route:{" "}
									{
										route.data.routeSections[0]
											.originationName
									}{" "}
									-{" "}
									{
										route.data.routeSections[0]
											.destinationName
									}
								</Typography>
							</Paper>
							<div className={classes.preloader}>
								<CircleLoader
									color={"#3f51b5"}
									loading={routeSequence.loading}
								/>
							</div>

							{routeSequence.data && <RouteSequenceComponent
								stopPoints={routeSequence.data.stations}
							/>}
						</div>
					)}
				</div>
			);
		}
		return result;
	}
}

Route.propTypes = {
	classes: PropTypes.object.isRequired
};

export const RouteComponent = withStyles(styles)(Route);
