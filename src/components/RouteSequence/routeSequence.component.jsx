import React, { Component } from "react";
import { StopPointItemComponent } from "../StopPointItem/stopPointItem.component";

import Table, {
	TableBody,
	TableCell,
	TableHead,
	TableRow
} from "material-ui/Table";
import Paper from "material-ui/Paper";
import { withStyles } from "material-ui/styles";
import PropTypes from "prop-types";

const styles = theme => ({
	root: {
		width: "70vw",
		[theme.breakpoints.down("sm")]: {
			width: "95%"
		},
		margin: "0 auto",
		marginTop: theme.spacing.unit * 3,
		overflowX: "auto",
		align: "center"
	},
	table: {
		width: "85%"
	}
});
class RouteSequence extends Component {
	render() {
		const { stopPoints, classes } = this.props;
		return (
			<div>
				{stopPoints && (
					<Paper className={classes.root}>
						<Table className={classes.table}>
							<TableHead>
								<TableRow>
									<TableCell>Station</TableCell>
									<TableCell>Bus numbers</TableCell>
								</TableRow>
							</TableHead>
							<TableBody>
								{stopPoints.map(stopPoint => (
									<StopPointItemComponent
										stopPoint={stopPoint}
										key={stopPoint.id}
									/>
								))}
							</TableBody>
						</Table>
					</Paper>
				)}
				{/* <ul>
					{stopPoints &&
						stopPoints.map(stopPoint => (
							<StopPointItemComponent
								stopPoint={stopPoint}
								key={stopPoint.id}
							/>
						))}
				</ul> */}
			</div>
		);
	}
}

RouteSequence.propTypes = {
	classes: PropTypes.object.isRequired
};

export const RouteSequenceComponent = withStyles(styles)(RouteSequence);
