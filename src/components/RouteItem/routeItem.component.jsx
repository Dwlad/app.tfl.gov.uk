import React, { PureComponent } from "react";
import NavLink from "react-router-dom/NavLink";

import { TableCell, TableRow } from "material-ui/Table";

export class RouteItemComponent extends PureComponent {
	render() {
		const { route } = this.props;
		return (
			<TableRow key={route.id}>
				<TableCell>
					<NavLink to={`/route/${route.id}`}>{route.name}</NavLink>
				</TableCell>
				<TableCell>
					<NavLink
						to={`/stoppoint/${route.routeSections[0].originator}`}
					>
						{route.routeSections[0].originationName}
					</NavLink>
				</TableCell>
				<TableCell>
					<NavLink
						to={`/stoppoint/${route.routeSections[0].destination}`}
					>
						{route.routeSections[0].destinationName}
					</NavLink>
				</TableCell>
			</TableRow>

			// <div>
			// 	<NavLink to={`/route/${route.id}`}>{route.name}</NavLink>
			// 	<div>
			// 		<NavLink to={`/stoppoint/${route.routeSections[0].originator}`}>{route.routeSections[0].originationName}</NavLink> -{" "}
			// 		<NavLink to={`/stoppoint/${route.routeSections[0].destination}`}>{route.routeSections[0].destinationName}</NavLink>
			// 	</div>
			// </div>
		);
	}
}
