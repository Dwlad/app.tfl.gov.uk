import React, { Component } from "react";
import NavLink from "react-router-dom/NavLink";
import { TableCell, TableRow } from "material-ui/Table";
export class StopPointItemComponent extends Component {
	render() {
		const { stopPoint } = this.props;
		return (
			<TableRow key={stopPoint.id}>
				<TableCell>
					<NavLink to={`/stoppoint/${stopPoint.id}`}>
						{stopPoint.name}
					</NavLink>
				</TableCell>
				<TableCell>
					{stopPoint.lines.map((line, i, arr) => (
						<span key={line.id}>
							<NavLink to={`/route/${line.id}`}>
								{line.name}
							</NavLink>
							{i < (arr.length - 1) ? ", " : ""}
						</span>
					))}
				</TableCell>
			</TableRow>

			// <li key={stopPoint.id}>
			// 	<NavLink to={`/stoppoint/${stopPoint.id}`}>
			// 		{stopPoint.name}
			// 	</NavLink>{" "}
			// 	Bus numbers:{" "}
			// 	{stopPoint.lines.map((line, i, arr) => (
			// 		<span key={line.id}>
			// 			<NavLink to={`/route/${line.id}`}>{line.name}</NavLink>
			// 			{i < arr.length ? ", " : ""}
			// 		</span>
			// 	))}
			// </li>
		);
	}
}
