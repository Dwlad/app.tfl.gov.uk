import React from "react";
import { withGoogleMap, GoogleMap, Marker } from "react-google-maps";

export const MapsComponent = withGoogleMap(props => (
	<GoogleMap
		defaultZoom={17}
		defaultCenter={{ lat: props.lat, lng: props.lng }}
	>
		<Marker position={{ lat: props.lat, lng: props.lng }} />
	</GoogleMap>

));