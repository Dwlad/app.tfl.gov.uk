import React, { PureComponent } from "react";
import { RouteItemComponent } from "../RouteItem/routeItem.component";
import Table, {
	TableBody,
	TableCell,
	TableHead,
	TableRow
} from "material-ui/Table";
import Paper from "material-ui/Paper";
import { withStyles } from "material-ui/styles";
import PropTypes from "prop-types";
import { CircleLoader } from "react-spinners";
import TextField from "material-ui/TextField";

const styles = theme => ({
	root: {
		width: "70vw",
		[theme.breakpoints.down("sm")]: {
			width: "95%"
		},
		margin: "0 auto",
		marginTop: theme.spacing.unit * 3,
		overflowX: "auto",
		align: "center"
	},
	table: {
		minWidth: 500
	},
	preloader: {
		display: "flex",
		flex: "0 0 auto",
		alignItems: "center",
		justifyContent: "center",
		margin: "20px"
	},
	container: {
		display: "flex",
		flexWrap: "wrap"
	},
	textField: {
		width: "100%"
	},
	menu: {
		width: 200
	}
});
class RouteList extends PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			searchPhrase: ""
		};
	}
	componentWillMount() {
		this.props.routesActions.getAllRoutes();
	}

	componentWillUnmount() {
		this.props.routesActions.clearRoutes();
	}

	search(searchPhrase) {
		console.log(searchPhrase);
		this.setState({ searchPhrase });
	}
	render() {
		const { routes, classes } = this.props;
		const { searchPhrase } = this.state;
		return (
			<div>
				<div className={classes.preloader}>
					<CircleLoader color={"#3f51b5"} loading={routes.loading} />
				</div>

				{routes.data.length > 0 && (
					<div  className={classes.root}>
						<form
							className={classes.container}
							noValidate
							autoComplete="off"
						>
							<TextField
								id="name"
								label="Search"
								className={classes.textField}
								value={this.state.searchPhrase}
								onChange={e => this.search(e.target.value)}
								margin="normal"
							/>
						</form>
						<Paper>
							<Table className={classes.table}>
								<TableHead>
									<TableRow>
										<TableCell>Route</TableCell>
										<TableCell>Origination</TableCell>
										<TableCell>Destination</TableCell>
									</TableRow>
								</TableHead>
								<TableBody>
									{routes.data
										.filter(
											route =>
												route.id.includes(
													searchPhrase
												) ||
												route.routeSections[0].originationName.includes(
													searchPhrase
												) ||
												route.routeSections[0].destinationName.includes(
													searchPhrase
												)
										)
										.map(route => (
											<RouteItemComponent
												key={route.id}
												route={route}
											/>
										))}
								</TableBody>
							</Table>
						</Paper>
					</div>
				)}
			</div>
		);
	}
}

RouteList.propTypes = {
	classes: PropTypes.object.isRequired
};

export const RouteListComponent = withStyles(styles)(RouteList);
