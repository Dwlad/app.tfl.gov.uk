import React, { Component } from 'react';

export class ErrorBoundaryComponent extends Component {
    state = {
        hasError: false
    };

    componentDidCatch(error, info) {
        console.log('error', error);
        console.log('info', info);
        this.setState({ hasError: true });
    }

    render() {
        if (this.state.hasError) {
            return (
                <div>
                    <p>Sorry! An accured error:(</p>
                    <button onClick={() => console.log('Error message: ')}>Show message</button>
                </div>
            );
        } else {
            return this.props.children;
        }
    }
}