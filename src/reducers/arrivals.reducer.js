import {
	CLEAR_ARRIVALS,GET_ARRIVALS_ERROR,GET_ARRIVALS_REQUEST,GET_ARRIVALS_SUCCESS
} from "../constants/arrivals.constants";

const initialState = {
	data: [],
	error: null,
	loading: false
};

export function arrivalsReducer(state = initialState, action) {
	switch (action.type) {
		case GET_ARRIVALS_REQUEST:
			return {
				...state,
				loading: true,
				error: undefined
			};
		case GET_ARRIVALS_SUCCESS:
			return {
				...state,
				loading: false,
				data: action.payload,
				error: undefined
			};
		case GET_ARRIVALS_ERROR:
			return {
				...state,
				loading: false,
				error: action.error
			};
		case CLEAR_ARRIVALS:
			return {
				...state,
				loading: false,
				data: state.data,
				error: action.error
			};
		default:
			return state;
	}
}
