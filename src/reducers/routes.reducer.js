import {
	GET_ROUTES_ERROR,
	GET_ROUTES_REQUEST,
	GET_ROUTES_SUCCESS,
	CLEAR_ROUTES
} from "../constants/routes.constants";

const initialState = {
	data: [],
	error: null,
	loading: false
};

export function routesReducer(state = initialState, action) {
	switch (action.type) {
		case GET_ROUTES_REQUEST:
			return {
				...state,
				loading: true,
				error: undefined
			};
		case GET_ROUTES_SUCCESS:
			return {
				...state,
				loading: false,
				data: action.payload.sort((a, b) => {
					const aIdInt = Number.parseInt(a.id, 10);
					const bIdInt = Number.parseInt(b.id, 10);
					if (!Number.isNaN(aIdInt) && !Number.isNaN(bIdInt)) {
						if (aIdInt < bIdInt) {
							return -1;
						}
						if (aIdInt > bIdInt) {
							return 1;
						}
						return 0;
					}
					if (a.id < b.id) {
						return -1;
					}
					if (a.id > b.id) {
						return 1;
					}
					return 0;
				}),
				error: undefined
			};
		case GET_ROUTES_ERROR:
			return {
				...state,
				loading: false,
				data: state.data,
				error: action.error
			};
		case CLEAR_ROUTES:
			return {
				...state,
				loading: false,
				error: action.error
			};
		default:
			return state;
	}
}
