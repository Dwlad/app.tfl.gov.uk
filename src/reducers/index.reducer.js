import { combineReducers } from "redux";
import { routeReducer } from "./route.reducer";
import { routesReducer } from "./routes.reducer";
import { routeSequenceReducer } from "./routeSequence.reducer";
import { stopPointReducer } from "./stopPoint.reducer";
import { arrivalsReducer } from "./arrivals.reducer";

export default combineReducers({
	route: routeReducer,
	routes: routesReducer,
	routeSequence:routeSequenceReducer,
	stopPoint:stopPointReducer,
	arrivals:arrivalsReducer
});
