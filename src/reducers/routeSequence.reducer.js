import {
	GET_ROUTE_SEQUENCE_BY_LINE_ERROR,
	CLEAR_ROUTE_SEQUENCE,
	GET_ROUTE_SEQUENCE_BY_LINE_REQUEST,
	GET_ROUTE_SEQUENCE_BY_LINE_SUCCESS
} from "../constants/routeSequence.constants";

const initialState = {
	data: [],
	error: null,
	loading: false
};

export function routeSequenceReducer(state = initialState, action) {
	switch (action.type) {
		case GET_ROUTE_SEQUENCE_BY_LINE_REQUEST:
			return {
				...state,
				loading: true,
				error: undefined
			};
		case GET_ROUTE_SEQUENCE_BY_LINE_SUCCESS:
			return {
				...state,
				loading: false,
				data: action.payload,
				error: undefined
			};
		case GET_ROUTE_SEQUENCE_BY_LINE_ERROR:
			return {
				...state,
				loading: false,
				error: action.error
			};
		case CLEAR_ROUTE_SEQUENCE:
			return {
				...state,
				loading: false,
				data: state.data,
				error: action.error
			};
		default:
			return state;
	}
}
