import {
GET_STOP_POINT_ERROR,CLEAR_STOP_POINT,GET_STOP_POINT_REQUEST,GET_STOP_POINT_SUCCESS
} from "../constants/stopPoint.constants";

const initialState = {
	data: undefined,
	error: undefined,
	loading: false
};

export function stopPointReducer(state = initialState, action) {
	switch (action.type) {
		case GET_STOP_POINT_REQUEST:
			return {
				...state,
				loading: true,
				error: undefined
			};
		case GET_STOP_POINT_SUCCESS:
			return {
				...state,
				loading: false,
				data: action.payload,
				error: undefined
			};
		case GET_STOP_POINT_ERROR:
			return {
				...state,
				loading: false,
				error: action.error
			};
		case CLEAR_STOP_POINT:
			return {
				...state,
				loading: false,
				data: undefined,
				error: undefined
			};
		default:
			return state;
	}
}
