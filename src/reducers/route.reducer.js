import {
	GET_ROUTE_ERROR,
	GET_ROUTE_REQUEST,
	GET_ROUTE_SUCCESS,
	CLEAR_ROUTE
} from "../constants/route.constants";

const initialState = {
	data: undefined,
	error: undefined,
	loading: false
};

export function routeReducer(state = initialState, action) {
	switch (action.type) {
		case GET_ROUTE_REQUEST:
			return {
				...state,
				loading: true,
				error: undefined
			};
		case GET_ROUTE_SUCCESS:
			return {
				...state,
				loading: false,
				data: action.payload,
				error: undefined
			};
		case GET_ROUTE_ERROR:
			return {
				...state,
				loading: false,
				error: action.error
			};
		case CLEAR_ROUTE:
			return {
				...state,
				loading: false,
				data: undefined,
				error: undefined
			};
		default:
			return state;
	}
}
