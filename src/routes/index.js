import React from "react";
import { Route, Switch } from "react-router-dom";
import AppContainer from "../containers/App/App";
export const App = () => (
	<div>
		<Switch>
			<Route render={() => <AppContainer />} />
		</Switch>
	</div>
);
