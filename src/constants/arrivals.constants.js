export const CLEAR_ARRIVALS = "CLEAR_ARRIVALS";

export const GET_ARRIVALS_REQUEST = "GET_ARRIVALS_REQUEST";
export const GET_ARRIVALS_SUCCESS = "GET_ARRIVALS_SUCCESS";
export const GET_ARRIVALS_ERROR = "GET_ARRIVALS_ERROR";
