export const GET_ROUTE_SEQUENCE_BY_LINE_REQUEST = "GET_ROUTE_SEQUENCE_BY_LINE_REQUEST";
export const GET_ROUTE_SEQUENCE_BY_LINE_SUCCESS = "GET_ROUTE_SEQUENCE_BY_LINE_SUCCESS";
export const GET_ROUTE_SEQUENCE_BY_LINE_ERROR = "GET_ROUTE_SEQUENCE_BY_LINE_ERROR";

export const CLEAR_ROUTE_SEQUENCE = "CLEAR_ROUTE_SEQUENCE";