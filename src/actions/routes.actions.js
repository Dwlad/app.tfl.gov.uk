import { URL } from "../helpers/url/index";
import {
	GET_ROUTES_ERROR,
	GET_ROUTES_SUCCESS,
	GET_ROUTES_REQUEST,
	CLEAR_ROUTES
} from "../constants/routes.constants";

export function getAllRoutes() {
	return dispatch => {
		dispatch({ type: GET_ROUTES_REQUEST });
		URL.request(`/Line/Mode/bus/Route`)
			.then(data => dispatch({ type: GET_ROUTES_SUCCESS, payload: data }))
			.catch(error => dispatch({ type: GET_ROUTES_ERROR, error: error }));
	};
}

export function clearRoutes(id) {
	return dispatch => dispatch({ type: CLEAR_ROUTES });
}
