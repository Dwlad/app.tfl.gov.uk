import {
	CLEAR_ARRIVALS,
	GET_ARRIVALS_ERROR,
	GET_ARRIVALS_REQUEST,
	GET_ARRIVALS_SUCCESS
} from "../constants/arrivals.constants";
import { URL } from "../helpers/url/index";

export function getArrivalForLineByStopPoint(lineId) {
	return dispatch => {
		dispatch({ type: GET_ARRIVALS_REQUEST });
		URL.request(`/StopPoint/${lineId}/Arrivals`)
			.then(data =>
				dispatch({ type: GET_ARRIVALS_SUCCESS, payload: data })
			)
			.catch(error =>
				dispatch({ type: GET_ARRIVALS_ERROR, error: error })
			);
	};
}
export function clearArrivals() {
	return dispatch => dispatch({ type: CLEAR_ARRIVALS });
}
