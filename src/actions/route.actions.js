import { URL } from "../helpers/url/index";
import {
	GET_ROUTE_ERROR,
	GET_ROUTE_SUCCESS,
	GET_ROUTE_REQUEST,
	CLEAR_ROUTE
} from "../constants/route.constants";

export function getRouteById(id) {
	return dispatch => {
		dispatch({ type: GET_ROUTE_REQUEST });
		URL.request(`/Line/${id}/Route`)
			.then(data => dispatch({ type: GET_ROUTE_SUCCESS, payload: data }))
			.catch(error => dispatch({ type: GET_ROUTE_ERROR, error: error }));
	};
}

export function clearRoute(id) {
	return dispatch => dispatch({ type: CLEAR_ROUTE });
}
