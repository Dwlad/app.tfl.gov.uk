import {
	GET_STOP_POINT_REQUEST,
	GET_STOP_POINT_SUCCESS,
	GET_STOP_POINT_ERROR,
	CLEAR_STOP_POINT
} from "../constants/stopPoint.constants";
import { URL } from "../helpers/url/index";

export function getStopPointById(stopPointId) {
	return dispatch => {
		dispatch({ type: GET_STOP_POINT_REQUEST });
		URL.request(`/StopPoint/${stopPointId}`)
			.then(data =>
				dispatch({ type: GET_STOP_POINT_SUCCESS, payload: data })
			)
			.catch(error =>
				dispatch({ type: GET_STOP_POINT_ERROR, error: error })
			);
	};
}

export function clearStopPoint() {
	return dispatch => dispatch({ type: CLEAR_STOP_POINT });
}
