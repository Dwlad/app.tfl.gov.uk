import {
	CLEAR_ROUTE_SEQUENCE,
	GET_ROUTE_SEQUENCE_BY_LINE_ERROR,
	GET_ROUTE_SEQUENCE_BY_LINE_REQUEST,
	GET_ROUTE_SEQUENCE_BY_LINE_SUCCESS
} from "../constants/routeSequence.constants";
import { URL } from "../helpers/url/index";

export function getRouteSequenceByLine(lineId, direction = "outbound") {
	return dispatch => {
		dispatch({ type: GET_ROUTE_SEQUENCE_BY_LINE_REQUEST });
		URL.request(`/Line/${lineId}/Route/Sequence/${direction}`)
			.then(data =>
				dispatch({ type: GET_ROUTE_SEQUENCE_BY_LINE_SUCCESS, payload: data })
			)
			.catch(error =>
				dispatch({ type: GET_ROUTE_SEQUENCE_BY_LINE_ERROR, error: error })
			);
	};
}

export function clearRouteSequence(id) {
	return dispatch => dispatch({ type: CLEAR_ROUTE_SEQUENCE });
}
