import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import {App} from "./routes";
import registerServiceWorker from "./registerServiceWorker";
import { Provider } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import configureStore from "./store/index.store";

const initialState = Window ? Window["REDUX_INITIAL_STATE"] : {};
const store = configureStore(initialState);
if (Window && Window["REDUX_INITIAL_STATE"]) {
	Window["REDUX_INITIAL_STATE"] = null;
}

const render = Component => {
	try {
		ReactDOM.render(
			<Provider key={module.hot ? Date.now() : store} store={store}>
				<BrowserRouter>
					<Component />
				</BrowserRouter>
			</Provider>,
			document.getElementById("root")
		);
	} catch (err) {
		console.error(err);
	}
};

render(App);

registerServiceWorker();
