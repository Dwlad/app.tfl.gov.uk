import "./App.css";

import * as Redux from "redux";

import { NavLink, Route, Switch, withRouter } from "react-router-dom";
import React, { PureComponent } from "react";
import { clearRoute, getRouteById } from "../../actions/route.actions";
import {
	clearRouteSequence,
	getRouteSequenceByLine
} from "../../actions/routeSequence.actions";
import { clearRoutes, getAllRoutes } from "../../actions/routes.actions";
import {
	clearStopPoint,
	getStopPointById
} from "../../actions/stopPoint.actions";
import {
	getArrivalForLineByStopPoint,
	clearArrivals
} from "../../actions/arrivals.actions";

import { PassPropsToChild } from "../../helpers/passPropsToChild/index";
import { RouteComponent } from "../../components/Route/route.component";
import { RouteListComponent } from "../../components/RouteList/routesList.component";
import { StopPointComponent } from "../../components/StopPoint/stopPoint.component";
import { connect } from "react-redux";

import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import Typography from "material-ui/Typography";
import Button from "material-ui/Button";
import { ErrorBoundaryComponent } from "../../components/ErrorBoundary/errorBoundary.component";
class AppContainer extends PureComponent {
	render() {
		return (
			<div className="App">
				<ErrorBoundaryComponent>
					<AppBar position="static">
						<Toolbar>
							<Typography
								type="title"
								color="inherit"
								className="flex"
							>
								Buses - Transport for London
							</Typography>
							<NavLink to="/route">
								<Button color="contrast">Routes</Button>
							</NavLink>
						</Toolbar>
					</AppBar>
					<Switch>
						<Route
							path="/"
							exact
							render={props =>
								PassPropsToChild(
									RouteListComponent,
									props,
									this.props
								)
							}
						/>
						<Route
							path="/route"
							exact
							render={props =>
								PassPropsToChild(
									RouteListComponent,
									props,
									this.props
								)
							}
						/>
						<Route
							path="/route/:id"
							exact
							render={props =>
								PassPropsToChild(
									RouteComponent,
									props,
									this.props
								)
							}
						/>
						<Route
							path="/stoppoint/:id"
							exact
							render={props =>
								PassPropsToChild(
									StopPointComponent,
									props,
									this.props
								)
							}
						/>
					</Switch>
				</ErrorBoundaryComponent>
			</div>
		);
	}
}

function mapStateToProps(state) {
	return {
		route: state.route,
		routes: state.routes,
		routeSequence: state.routeSequence,
		stopPoint: state.stopPoint,
		arrivals: state.arrivals
	};
}

const mapDispatchToProps = dispatch => {
	return {
		routeActions: Redux.bindActionCreators(
			{
				getRouteById,
				clearRoute
			},
			dispatch
		),
		routesActions: Redux.bindActionCreators(
			{
				getAllRoutes,
				clearRoutes
			},
			dispatch
		),
		routeSequenceActions: Redux.bindActionCreators(
			{
				getRouteSequenceByLine,
				clearRouteSequence
			},
			dispatch
		),
		stopPointActions: Redux.bindActionCreators(
			{
				clearStopPoint,
				getStopPointById
			},
			dispatch
		),
		arrivalsActions: Redux.bindActionCreators(
			{
				getArrivalForLineByStopPoint,
				clearArrivals
			},
			dispatch
		)
	};
};

const Connect = connect(mapStateToProps, mapDispatchToProps)(AppContainer);

export default withRouter(Connect);
