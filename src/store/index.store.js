import { applyMiddleware, compose, createStore } from "redux";

import Reducers from "../reducers/index.reducer";
import thunk from "redux-thunk";

export default function(initialState = {}) {
	let store = createStore(
		Reducers,
		initialState,
		compose(
			applyMiddleware(
				thunk /*.withExtraArgument({axios})*/
				/*, Logger()*/
			),
			process.env.NODE_ENV !== "production" && window.devToolsExtension
				? window.devToolsExtension()
				: f => f
		)
	);

	const NodeModule = module;

	if (NodeModule.hot) {
		NodeModule.hot.accept("../reducers/index.reducer", () => {
			store.replaceReducer(require("../reducers/index.reducer").default);
		});
	}
	return store;
}
